const stripe = require("stripe")(
  "sk_test_51JY2zPSGwPij7NDOL6sx1NGa4ZkgrbYBAqBjo1hHwL4h5Yn773TP9ItvZBGbv6xDuAIEMqOwhVpFA8CITXYr4SCe00jyOcSRdc"
);
const uuid = require("uuid/v4");

exports.standardSubscription = (req, res) => {
  const { product, token } = req.body;
  console.log("Product", product);
  console.log("Price", product.price);

  const iKey = uuid();

  return stripe.customers
    .create({
      email: token.email,
      source: token.id,
    })
    .then((customer) => {
      stripe.charges.create({
        amount: product.price * 1000,
        currency: "inr",
        customer: customer.id,
        receipt_email: token.email,
        description: `Product name`,
        shipping: {
          name: token.card.name,
          address: {
            country: token.card.address_country,
          },
        },
      });
    })
    .then((result) => res.status(200).json(result))
    .catch((err) => {
      console.log(err);
    });
};

exports.getData = (req, res) => {
  res.send("Inside premium");
};
