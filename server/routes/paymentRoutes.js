const express = require("express");
const routes = express.Router();
const payment = require("../controllers/paymentController");

routes.post("/premium", payment.standardSubscription);
routes.get("/premium", payment.getData);

module.exports = routes;
