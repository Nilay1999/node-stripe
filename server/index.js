const stripe = require("stripe")("");
const express = require("express");
const app = express();
const cors = require("cors");
const routes = require("./routes/paymentRoutes");

app.use(express.json());
app.use(cors());

app.get("/", (req, res) => {
  res.send("Server is running !");
});

app.listen(8080, () => {
  console.log(`Server running on 8080 .... `);
});

app.use("/", routes);
